#!/usr/bin/env python3

import z3

from enum import Enum


s = z3.Optimize()


def at_most(bools, max_num):
    s.add(z3.Sum([z3.If(b, 1, 0) for b in bools]) <= max_num)


def at_least(bools, min_num):
    s.add(z3.Sum([z3.If(b, 1, 0) for b in bools]) >= min_num)


class SMTUsedVar(object):
    def __init__(self, cannonical_name: str):
        self.cannonical_name = cannonical_name

        # register SMT variable
        self.smt_used = z3.Bool("used_" + self.cannonical_name)

    @property
    def is_used(self, is_used):
        s.add(self.smt_used == is_used)

    def add_smt_asserts():
        pass


class BlockType(Enum):
    # Undefined
    UNDEFINED = -1

    # Microcontroller
    MCU = 0

    # Power distributor (voltage regulator,...)
    POWER = 1

    # sensors/interfaces
    SENSOR_POSITION_ABSOULTE_GPS = 2
    SENSOR_POSITION_RELATIVE_IMU = 3
    SENSOR_ENVIROMENT_TEMPERATUR = 4
    SENSOR_ENVIROMENT_HUMIDITY = 5
    SENSOR_ENVIROMENT_PRESSURE = 6

    COM_RF_GSM = 7
    COM_RF_WLAN = 8
    COM_RF_BLUETOOTH = 9
    COM_RF_LORA = 10
    COM_CABLE_USB = 11
    COM_CABLE_UART = 12
    COM_CABLE_ETHERNET = 13

    MEMORY_STATIC_EEPROM = 14
    MEMORY_STATIC_FLASH = 15
    MEMORY_DYNAMIC_DRAM = 16

    OUTPUT_VISUAL_LED = 17
    OUTPUT_VISUAL_DISPLAY_LCD = 18
    OUTPUT_VISUAL_DISPLAY_OLED = 19


SOLVER_BLOCK_TYPES = {}


class Block(SMTUsedVar):
    def __init__(self, name: str, idx: int, block_types):
        super(Block, self).__init__(f"block/{name}/{idx}")
        self.name: str = name
        self.idx: int = idx
        self.block_types = block_types

        self.pins = []

        # add SMT assert-soft
        s.add_soft(self.smt_used == False, id='BLOCKS')

        # add SMT assert
        for b in self.block_types:
            if b not in SOLVER_BLOCK_TYPES:
                SOLVER_BLOCK_TYPES[b] = [self.smt_used]
            else:
                SOLVER_BLOCK_TYPES[b].append(self.smt_used)

    def add_pin(self, pin):
        self.pins.append(pin)

    def add_smt_asserts(self):
        for p in self.pins:
            p.add_smt_asserts()


class Pin(SMTUsedVar):
    def __init__(self, block: Block, pin: str, required: bool = False):
        super(Pin, self).__init__(f"{block.cannonical_name}/{pin}")
        self.block: Block = block
        self.pin: str = pin
        self.required: bool = required

        self.block.add_pin(self)

        # add SMT assert-soft
        s.add_soft(self.smt_used == False, id='PINS')

        if self.required:
            s.add(self.smt_used == block.smt_used)
        else:
            # add SMT assert
            s.add(z3.Implies(self.smt_used, block.smt_used))

        self.pin_configs = []

    def add_pin_config(self, pin_config):
        self.pin_configs.append(pin_config)

    def add_smt_asserts(self):
        if len(self.pin_configs) > 1:
            # pin is used when at least one pin type is used
            s.add(self.smt_used == z3.Or([v.smt_used for v in self.pin_configs]))
            # at most one type can be activated
            at_most([v.smt_used for v in self.pin_configs], 1)
        elif len(self.pin_configs) == 1:
            # shortcut in case of only one type
            s.add(self.smt_used == self.pin_configs[0].smt_used)
        else:
            raise AssertionError('at least one configuration expected!')

        for p in self.pin_configs:
            p.add_smt_asserts()


class PinConfigType(Enum):
    # Power
    P_OUTPUT = 0
    P_INPUT = 1

    # Analog
    A_OUTPUT = 2
    A_INPUT = 3

    # Digital
    D_INPUT = 4
    D_OUTPUT = 5

    # Digit Pulse-Width modulation
    D_PWM_INPUT = 6
    D_PWM_OUTPUT = 7

    # Digital Interrupt (Edge)
    D_INT_EDGE_INPUT = 8
    D_INT_EDGE_OUTPUT = 9

    # Digital I2C
    D_I2C_SDA_MASTER = 10
    D_I2C_SCL_MASTER = 11
    D_I2C_SDA_SLAVE = 12
    D_I2C_SCL_SLAVE = 13


# Link PinConfigTypes together (what can be considered a valid net) source: [sink]
CONFIG_LINK_MATRIX = {
    PinConfigType.P_OUTPUT: [PinConfigType.P_INPUT],
    PinConfigType.A_OUTPUT: [PinConfigType.A_INPUT],
    PinConfigType.D_OUTPUT: [PinConfigType.D_INPUT],
    PinConfigType.D_PWM_OUTPUT: [PinConfigType.D_PWM_INPUT],
    PinConfigType.D_INT_EDGE_OUTPUT: [PinConfigType.D_INT_EDGE_INPUT],
    PinConfigType.D_I2C_SDA_MASTER: [PinConfigType.D_I2C_SDA_SLAVE],
    PinConfigType.D_I2C_SCL_MASTER: [PinConfigType.D_I2C_SCL_SLAVE]
}

# TODO: some links have 1:n, some have 1:1
# TODO: link pins together (I2C, UART, USB, SPI) as well as same power net
# TODO: i2c for example has adresses which need to be tracked


class PinConfig(SMTUsedVar):
    def __init__(self, pin: Pin, config: PinConfigType):
        super(PinConfig, self).__init__(f"{pin.cannonical_name}/{config.name}")
        self.pin: Pin = pin
        self.config: PinConfigType = config

        self.pin.add_pin_config(self)

        self.connections = []

        # add SMT assert
        s.add(z3.Implies(self.smt_used, pin.smt_used))

        self.connections = []

    def add_connection(self, connection):
        self.connections.append(connection)

    def add_smt_asserts(self):
        valid_connections = [c.smt_used for c in self.connections]
        s.add(z3.Implies(self.smt_used, z3.Or(valid_connections)))
        at_most(valid_connections, 1)


class Connection(SMTUsedVar):
    def __init__(self, source: PinConfig, sink: PinConfig):
        super(Connection, self).__init__(f"net/source-{source.cannonical_name}/sink-{sink.cannonical_name}")
        self.source: PinConfig = source
        self.sink: PinConfig = sink

        self.source.add_connection(self)
        self.sink.add_connection(self)

        # add SMT assert-soft
        s.add_soft(self.smt_used == False, id='NETS')

        # add SMT assert
        s.add(z3.Implies(self.smt_used, z3.And(source.smt_used, sink.smt_used)))


class ConnectionGroup(object):
    def __init__(self):
        self.connetions = []


def print_model(s):
    m = s.model()
    print("#### traversing model...")
    for d in sorted(m.decls(), key=lambda d: d.name()):
        print(f" * {d.name()} = {m[d]}")
    print("####")


def print_assertions(s):
    print("#### asserted constraints...")
    for c in s.assertions():
        print(f" * {c}")
    print("####")


def print_objectives(s):
    m = s.model()
    print("#### objectives...")
    for o in s.objectives():
        print(f" * {o.get_id()} = {m.evaluate(o)}")  # TODO: get ID of objective
    print("####")


def create_mcu(name, id):
    mcu = Block(name, id, [BlockType.MCU])

    p0 = Pin(mcu, 0)
    p0_di = PinConfig(p0, PinConfigType.D_INPUT)
    p0_do = PinConfig(p0, PinConfigType.D_OUTPUT)

    p1 = Pin(mcu, 1)
    p1_di = PinConfig(p1, PinConfigType.D_INPUT)
    p1_do = PinConfig(p1, PinConfigType.D_OUTPUT)

    p2 = Pin(mcu, 2)
    p2_di = PinConfig(p2, PinConfigType.D_INPUT)
    p2_do = PinConfig(p2, PinConfigType.D_OUTPUT)

    return mcu


def create_led(name, id):
    led = Block(name, id, [BlockType.OUTPUT_VISUAL_LED])

    p0 = Pin(led, 0, True)
    p0_di = PinConfig(p0, PinConfigType.D_INPUT)

    return led


if __name__ == '__main__':
    blocks = []
    blocks.append(create_mcu('ESP', 0))

    for i in range(3):
        blocks.append(create_led('LED', i))

    # add nets between all blocks
    # TODO: automat
    nets = [Connection(blocks[0].pins[0].pin_configs[1], blocks[1].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[0].pin_configs[1], blocks[2].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[0].pin_configs[1], blocks[3].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[1].pin_configs[1], blocks[1].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[1].pin_configs[1], blocks[2].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[1].pin_configs[1], blocks[3].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[2].pin_configs[1], blocks[1].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[2].pin_configs[1], blocks[2].pins[0].pin_configs[0]),
            Connection(blocks[0].pins[2].pin_configs[1], blocks[3].pins[0].pin_configs[0])
    ]

    # add additional rules which we can only add when the model is finished
    for b in blocks:
        b.add_smt_asserts()

    # our constrains (what we need)
    at_least(SOLVER_BLOCK_TYPES.get(BlockType.MCU, []), 1)
    at_least(SOLVER_BLOCK_TYPES.get(BlockType.OUTPUT_VISUAL_LED, []), 3)

    # our custom asserts

    # Output formula valid to push into solver
    print(s.sexpr())

    # print_assertions(s)

    print(s.check())
    print_model(s)
    print_objectives(s)
